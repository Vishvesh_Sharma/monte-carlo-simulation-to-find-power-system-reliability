%time unit considered is year;
clear;
clc;
%failure & Repair rate of generator, Transmission Line & weather
frg=36.5; rrg=365; 
frw=1/200*24*365; rrw=1/20*24*365; 
frt1=10; frt2=100; rrt=1/8*24*365; 

%time counter for process
t=0;
% variables representing the state of generators, transmission line &
% weather
stateg1=1; stateg2=1; stateg3=1;
statet1=1; statet2=1;
weatherst=1;
stateg4=1;
% Load Variable 
Loadst1=1;
% Counter for failures
CountF=0;
% Failure Time Calculate
timeF=0;
% Loss Of load for Current State 
CstateF=1;
% variable that count number of years
Cyear=0;

% Simulate Procces till it converge to 5%
for i=1:50000;
% Loadst1 reviewed for current load 
if Loadst1==1;
     load=40;
 elseif Loadst1==2;
     load=101;
 elseif Loadst1==3;
     load=201;
 elseif Loadst1==4;
     load=101;
 elseif Loadst1==5;
     load=40;
end

% before changing the current state record it for previous state preference
PstateF=CstateF;

% Feed Capacity generator123, Transmission lines & generator4
% Take care of maximum transmission line capacity
Feed123=(stateg1+stateg2+stateg3)*60;
Trans12=(statet1+statet2)*100;
FinalGT=min(Feed123,Trans12);
FinalFeed=FinalGT+stateg4*60;

%Finding the Loss of Load State
if load>FinalFeed
     CstateF=0;
else 
    CstateF=1;
end

if CstateF==0;
    if PstateF==1;
        %if previous state was succes and current is failure
        CountF=CountF+1;
    end
end

%Montecarlo Equation for next state change for all component 
%Take care of the current state/ repair Rate to be used or failure Rate?
Ran=rand;
    if stateg1==1;
Timechg1=-log(Ran)/frg;
    else
Timechg1=-log(Ran)/rrg;
    end
Ran=rand;
 if stateg2==1;
Timechg2=-log(Ran)/frg;
 else
Timechg2=-log(Ran)/rrg;
 end 
 Ran=rand;
 if stateg3==1;
Timechg3=-log(Ran)/frg;
 else
Timechg3=-log(Ran)/rrg;
 end

 Ran=rand;
% In case of Transmission Line weather is also considered
if weatherst==1;
    if statet1==1;
         Timecht1=-log(Ran)/frt1;
     else
         Timecht1=-log(Ran)/rrt;
     end
 elseif weatherst==0;
     if statet1==1;
         Timecht1=-log(Ran)/frt2;
     else
         Timecht1=-log(Ran)/rrt;
     end
end


 Ran=rand;
 if weatherst==1;
    if statet2==1;
         Timecht2=-log(Ran)/frt1;
     else
         Timecht2=-log(Ran)/rrt;
     end
 elseif weatherst==0;
     if statet2==1;
         Timecht2=-log(Ran)/frt2;
     else
         Timecht2=-log(Ran)/rrt;
     end
 end

 Ran=rand;
  if weatherst==1;
Timechw=-log(Ran)/frw;
  else
Timechw=-log(Ran)/rrw;
 end
         
Ran=rand;
 if stateg4==1;
Timechg4=-log(Ran)/frg;
 else
Timechg4=-log(Ran)/rrg;
 end

TtoCh=[Timechg1,Timechg2,Timechg3,Timecht1,Timecht2,Timechw,Timechg4];
Tmin=min(TtoCh);
Tmin=Tmin*24*365;
%Tmin is new time at which state of feeding side going to change

Tyear=t;
%Load is variable changes according to the time.
if Loadst1==5;
 % Check the state of load during the feeding state gets change
     changeLoad=fix(t/8);
        if 8*(changeLoad+1)>t+Tmin;
            t=t+Tmin;
        else
        Tmin=8*(changeLoad+1)-t;
        t=8*(changeLoad+1);
        end
else
   changeLoad=fix(t/4);
        if t+Tmin<4*(changeLoad+1);
            t=t+Tmin;
        else
        Tmin=4*(changeLoad+1)-t;
        t=4*(changeLoad+1);
        end
end  

% Again Check for loss of load during the load changing condition
   if CstateF==0;
    timeF=timeF+Tmin;  
   end

TotalYear=fix(t/(365*24));

if (Tyear<TotalYear*365*24)&(t>=TotalYear*365*24);
    Cyear=Cyear+1;
    I(Cyear)=timeF;
    U(Cyear)=CountF;
end

%Iteration for next state
if Tmin==Timechg1*24*365;
     if stateg1==1;
    stateg1=0;
     else
         stateg1=1;
     end
 elseif Tmin==Timechg2*24*365;
         if stateg2==1;
         stateg2=0;
         else 
             stateg2=1;
         end
 elseif Tmin==Timechg3*24*365;
          if stateg3==1;
             stateg3=0;
          else 
              stateg3=1;
          end
 elseif Tmin==Timecht1*24*365;
          if statet1==1;
             statet1=0;
          else 
              statet1=1;
          end
elseif Tmin==Timecht2*24*365;
          if statet2==1;
             statet2=0;
          else 
              statet2=1;
          end
 elseif Tmin==Timechw*24*365;
          if weatherst==1;
             weatherst=0;
          else 
              weatherst=1;
          end
  elseif Tmin==Timechg4*24*365;
          if stateg4==1;
             stateg4=0;
          else 
              stateg4=1;
          end

end
 
%choosing the next load state by dividing the time by 24 so we can get the
%state of the load
d=rem(t,24);
if d<4;
    Loadst1=1;
elseif d<8;
    Loadst1=2;
elseif d<12;
    Loadst1=3;
elseif d<16;
    Loadst1=4;
elseif d<24;
    Loadst1=5;
end
end
%After simulation we have to calculate the Cov value, so we calculate the
%failure time of each year
for k=1:TotalYear;
    if k==1;
    Timechw(k)=I(k);
    else
        Timechw(k)=I(k)-I(k-1);
    end
end
% to calcuate Cov, we calculate average failure rate which is Ih is this code
Ih=I(TotalYear)/TotalYear;
% we get Cov we calcuate the Var
z=0;
for s=1:TotalYear;
    z=z+(Ih-Timechw(s))^2;
end
Variance=z/TotalYear^2; 
probabilityLOL=I(TotalYear)/(TotalYear*24*365)
FrequencyFailure=U(Cyear)/TotalYear
Covariance=Variance^0.5/Ih*100